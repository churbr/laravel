<?php

namespace App\Http\Controllers;

use Request;
use Auth;
use App\Blog;                       // Model
use App\Http\Requests\BlogRequest;  // Controller Request File

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogsController extends Controller
{

    public function __construct() {

        /*
         * If not logged in, do not run everything except index (Which shows all blogs)
         */

        $this->middleware('blog', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = new Blog;
        $blogs = $blog::latest()->get();
        
        return view('blog.blogs', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $input = Request::all();
        
        $blog = new Blog;
        $blog->user_id = Auth::user()->id; // use Auth;
        $blog->title = $input['title'];
        $blog->body = $input['body'];
        $blog->save(); // returns a boolean value

        return redirect('blogs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = new Blog;
        $post = $blog::find($id);

        return view('blog.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = new Blog;
        $blog_entry = $blog->findOrFail($id);

        // if( $blog_entry->user_id !== Auth::user()->id ) {
        //     return redirect('profile');
        // }

        return view('blog.update')->with('blog', $blog_entry);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, $id)
    {
        $blog = Blog::findOrFail($id);

        $this->authorize('update', $blog); // Authorization
        $blog->update($request->all());

        return redirect('profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::findOrFail($id);

        $this->authorize('delete', $blog);
        Blog::destroy($id);

        return redirect('profile');
    }
}
