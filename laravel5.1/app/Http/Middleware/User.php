<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard; // So we can use the Guard class

use Closure;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected $auth;


    public function __construct(Guard $auth) {

        /*
            $this->auth : it's the 'protected $auth' within the class
            $auth       : is the Guard's object within the Guard.php class pass through parameters
        */

        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        /* Now, we can use guest() method, to check if the user is logged in */

        if($this->auth->guest()) {
            return redirect('/');
        }

        return $next($request);
    }
}
