<?php

Route::get('test', function() {
	echo '<h3>Test.</h3>';
	var_dump(Auth::check());
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
//		  ('URL', 'Controller@method')

Route::get('password/email', 'HomeController@password');

Route::resource('user', 'UserController');
/*	ROUTES: use route('user.method')
 *						user.store
 *						user.create
 *						user.show
 *						user.update
 *						user.destroy
 *						user.edit
 **/

Route::get('profile', ['as' => 'profile', 'uses' => 'UserController@profile']);

Route::resource('blogs', 'BlogsController');
Route::post('blog/{id}', 'BlogsController@update'); // MethodNotAllowedHttpException fixed

/*	ROUTES: use route('blogs.method')
 *						blogs.index
 *						blogs.store
 *						blogs.create
 *						blogs.show
 *						blogs.update
 *						blogs.destroy
 *						blogs.edit
 **/

// Authentication routes...
Route::get('auth/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

// Registration routes...
Route::get('auth/register', ['as' => 'register', 'uses' => 'Auth\AuthController@getRegister']);
Route::post('auth/register', 'Auth\AuthController@postRegister');