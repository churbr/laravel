@extends('main')
@section('title', 'Blog')
@section('container')


	@if(Auth::check())
		<a class="btn btn-primary" href="{{ route('blogs.create') }}" role="button">
			Create blog
		</a>
	@endif


	@forelse($blogs as $blog)
		
		<a href="{{ url('blogs', $blog['id']) }}">
			<h3> {{ $blog['title'] }} </h3>
		</a>

		<div class='entry-content'>
			{{ $blog['body'] }}
			
			<p>
				by:
					<em>
						<a href="{{ url('user', $blog['user_id']) }}">
							{{ $blog->user->name }}
						</a>
					</em>
			</p>
		</div>

		<hr />
	@empty
		<p class='text-danger'>No blog entry yet.</p>
	@endforelse

@endsection