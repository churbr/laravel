@extends('main')
@section('title', 'Create Blog')
@section('container')

	<div class='col-md-6'>
		<form action="{{url('blogs')}}" method="POST">

      <div class='form-group'>
        <h1>Add Blog</h1>
      </div>

      <div class='form-group'>
        <label for="title">Title</label>
         <input type="text" class="form-control" name="title" id="title" placeholder="Title" />
      </div>

      <div class='form-group'>
          <label for="body">Body</label>
          <textarea class="form-control" name="body" id="body" rows="3"></textarea>
      </div>

      <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
      <button type="submit" class="btn btn-primary">Submit</button>

      @include('errors.form_error')

		</form>
	</div>
@endsection