@extends('main')
@section('title', 'Blogs')
@section('container')

	<h3> {{ $post->title }} </h3>

	<div class='entry-content'>
		<blockquote>
			{{ $post->body }}
			<footer>
				<a href="{{ url('user', $post->user_id) }}">{{ $post->user->name }} </a>
			</footer>
		</blockquote>
	</div>

@endsection