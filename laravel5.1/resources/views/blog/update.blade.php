@extends('main')
@section('title', 'Update Blog')
@section('container')

	<div class="col-md-6">

		<div class='form-group'>
		  	<h1>Update - {{ $blog->title }} </h1>
		</div>

		{!! Form::model($blog,
		  	[
			  	'method' => 'PATH',
			  	'action' => ['BlogsController@update', $blog->id]
			])
		!!}

		  @include('partials.form', ['buttonText' => 'Update Blog'])

		  {!! Form::close() !!}

		  <div class='form-group'>
		  	{!! Form::model($blog,
		  		[
		  			'method' => 'DELETE',
		  			'action' => ['BlogsController@destroy', $blog->id]
		  		]) !!}

		  		{!! Form::submit('Delete Blog', ['class' => 'btn btn-danger']) !!}

		  	{!! Form::close() !!}
		  </div>

		  @include('errors.form_error')

	</div>

@endsection