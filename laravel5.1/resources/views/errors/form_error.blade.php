<div class="form-group" style="margin-top: 10px;">
	@if($errors->any())
	  @foreach($errors->all() as $error)
	    <p class='text-danger'> <code> {{ $error }} </code> </p>
	  @endforeach
	@endif
</div>