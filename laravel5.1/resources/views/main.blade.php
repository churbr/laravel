<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="{{{ asset('favicon.ico') }}}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-theme.min.css') }}" />
		<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
	</head>

	<body>

		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle Navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/">Laravel</a>
				</div>

				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li> <a href="/">HOME</a> </li>
						<li> <a href="{{ route('blogs.index') }}">BLOGS</a> </li>
					</ul>


					<ul class="nav navbar-nav navbar-right">
						@if(Auth::guest())
							<li><a href="{{ route('login') }}">Login</a></li>
							<li><a href="{{ route('register') }}">Register</a></li>
						@else
							<li class='dropdown'>
								<a href='#' class='dropdown-toggle' data-toggle='dropdown'> {{ Auth::user()->name }} <span class='caret'></span> </a>
								<ul class='dropdown-menu'>
									<li> <a href="{{ route('profile') }}">Profile</a> </li>
									<li> <a href="{{ route('logout') }}">Logout</a> </li>
								</ul>
							</li>
						@endif
					</ul>
				
				</div>
			</div>
		</nav>


		<div class="container">
			@yield('container')
		</div>
	</body>
</html>