@extends('main')
@section('title', 'Home')
@section('container')
	
	<h1>Welcome</h1>
	<p>
		Hi, my name is <kbd>{{ $creator }}</kbd> and I'm the one who create this site. Thank you for checking it out. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	</p>

@endsection