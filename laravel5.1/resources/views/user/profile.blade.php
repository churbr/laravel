@extends('main')
@section('title', 'Profile')
@section('container')

	<div class='col-md-12'>
		<h2>Hello, {{ Auth::user()->name }}! </h2>
		<a href="{{ route('blogs.create') }}">Create Blog</a>
		<hr />

		<h2> Blog List </h2>
		<ul>

			@forelse($posts as $post)
				<li>
					<a href="{{ route('blogs.show', $post->id) }}"> {{ $post->title }} </a>
					[ <a class='bg-success' href="{{ route('blogs.edit', $post->id) }}">edit</a> ]
				</li>
			@empty
				<li class='bg-danger'>No post</li>
			@endforelse

		</ul>
	</div>

@endsection