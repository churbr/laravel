@extends('main')
@section('title', $profile->name)
@section('container')

	<h1>{{ $profile->name }}</h1>
	
	<hr />

	@forelse($posts as $post)
		<h4> <a href="{{ url('blogs', $post->id) }}"> {{ $post->title }} </a> </h4>
	@empty

	@endforelse

@endsection