<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use View;

class MainController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        // $this->middleware('auth');
    }
    
    public function index()
    {
        if(View::exists('pages.home')) {
            return view('pages.home');
        }else { abort(404); }
    }

    public function about() {
        return view('pages.about');
    }

    public function contact() {
        return view('pages.contact');
    }

    public function userlist() {
        $users = User::paginate(10);
        return view('pages.userlist', compact('users'));
    }

    public function test() {
        $user = new User;
        return $user::all();
    }

}
