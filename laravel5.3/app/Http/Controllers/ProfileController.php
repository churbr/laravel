<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
	public function index() {
       return view('pages.profile');
	}

    public function profile($username) {
    	// User::where('username', $username)->first();
    	// User::where('username', '=', $username)->first();    	
    	$user = User::whereUsername($username)->first();

    	if($user) {
    		return view('profile.view', compact('user'));
    	}else { abort('404'); }

    }

}
