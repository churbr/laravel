<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

/**
 *  This is the MyMiddleware class, I created it via `php artisan make:middleware MyMiddleware`
 *  After this middleware class is created, I added to handle function:
 *
 *       if(Auth::guest()) {
 *           return redirect('login');
 *       }
 *
 *  So that if someone goes to a link that is applied with this middleware,
 *  it will automatically redirect to login page if user is not logged in.
 **/

class MyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guest()) { // use Auth
            return redirect('login');
        }

        return $next($request);
    }
}
