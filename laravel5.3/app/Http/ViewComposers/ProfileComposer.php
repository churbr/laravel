<?php

namespace App\Http\ViewComposers;
use Illuminate\View\View;

class ProfileComposer {


	/**
	 *	And this is the ProfileComposer.php file, this is where we get the data that
	 *	we use for specific views. This file is like a storage of data for a specific view(s).
	 **/


	public function compose(View $view) {
		$view
			->with('dev_date', 'December 17, 2016')
			->with('dev_time', '01:44 AM')
			->with('phone_no', '09231459569')
			->with([
				'frameworks' => [
					'Laravel',
					'Code Igniter',
					'.NET Framework'
				]
			]);
	}

    /*
        If you want View composer to execute immediately, instead of waiting until the view
        is about to render. Then use View creator, to do that:
        1) Go to your ComposerServiceProvider.php file and change View::composer to View::creator
        2) And within this file (ProfileComposer.php), change method name from compose() to create().
    */

}