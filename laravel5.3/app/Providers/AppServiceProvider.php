<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

/* My imports */

use Auth;
use View;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $age = Carbon::createFromDate(1991, 04, 03)->age;

        /* This shares the '$owner' and '$myAge' variables to everywhere in your app */

        View::share('owner', 'Chu, Reynald Bryan R.');
        View::share('myAge', $age);

        /* We can't use 'View::share('auth', Auth::user())'.
         * So, we are using View::composer() method.
         * The '*' means the variable will be accessible to all views.
         * And the closure is used to pass the data.
         */

        View::composer('*', function($view) {
            $view->with('auth', Auth::user());
        });

        /* --- My Custom Directives --- */

        Blade::directive('age', function($expression) {
            $data = json_decode($expression);

            // array:3 [▼
            //   0 => 1991
            //   1 => 4
            //   2 => 3
            // ]

            $year = $data[0];
            $month = $data[1];
            $day = $data[2];

            $cd_age = Carbon::createFromDate($year, $month, $day)->age;

            return "<?php echo $cd_age; ?>";
        });

        // ---------------------------------------------------------------------------------

        Blade::directive('nice_name', function($expression) {
            return "
                <?php
                    echo <<<DDD
                        <span style='font-weight:bold; color:#0072BB;'>$expression</span>
DDD;
            ?>
            ";
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // 
    }
}
