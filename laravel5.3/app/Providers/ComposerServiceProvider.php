<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/* My imports */
use View;
use App\Http\ViewComposers;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * This is how you share variables on specific views.
         * Inside \App\Http\ViewComposers\ProfileComposer.php, there is a compose() method.
         * And it contains all the key & value pair or variables that is only accessible to 
         * the views specified in the first parameter e.g: 'pages.about' and 'pages.contact'.
         **/

        View::composer(['pages.about', 'pages.contact'], 'App\Http\ViewComposers\ProfileComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
