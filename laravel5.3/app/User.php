<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'username', // Custom attribute added
        'birthday', // Custom attribute added
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * This is a custom attribute, we should add this to protected $dates.
     * In order to avoid carbon date errors.
     * We can use it like: $user->birthday->format('l j F Y')
     * See: /resources/views/profile/view.blade.php, line: 12.
     *
     * @var array
     */

    protected $dates = [
        'birthday'
    ];


    /**
     * MUTATORS
     **/

    public function setNameAttribute($value) {
        
        /* Everytime a new data is to be inserted to the table,
            it will first capitalize the 1st letter of the attribute name */

        $this->attributes['name'] = ucfirst($value);
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
	 * ACCESSORS
     **/

    public function getNameAttribute($value) {
    	// So everytime we fetch this attribute from DB, it will automatically capitalize the name
    	return strtoupper($value);
    }
}
