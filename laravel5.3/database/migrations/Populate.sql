INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Bryan Chu', 'eminence43rd@gmail.com', '$2y$10$5kiP2Bl2DRrUzMPUeu/sd.IPAu1ruv3dxv4Pley7VTb0xJjobiK1u', '0HqBG5byhH2sB5np0pwPpjjhvKKfAsEnMTuv7xqCLqtTR8kFgKcb5yzMYkIu', '2016-12-10 10:38:32', '2016-12-16 12:22:22'),
(2, 'Bob Bejoc', 'sloteb@yahoo.com.ph', '$2y$10$kAeHUPFg6BpMdrSpxicVReoo0OShBIcUfmL0/bZ7STvhdgek5yD5C', 'oGevLEMI5kx9G203npctiWVW1yR9ebkKD86Xbdd9rMDuGtD1lBtRIS2TjFjT', '2016-12-10 10:39:51', '2016-12-14 16:58:27'),
(3, 'Jason Roxas', 'jazrox@ymail.com', '$2y$10$826VEuRyt9GjeV7Jx81qQevIhsoS6OH0B3Lnd1EpXCtjdOISW9cim', NULL, '2016-12-10 10:40:44', '2016-12-10 10:40:44'),
(4, 'Nikkie R. Chu', 'nix_chix@ymail.com', '$2y$10$.jSJurkZoOvAnqftvr8g.O./BLlJvj4J6V6aMB9XGOuQOpb2ZMHJ.', NULL, '2016-12-10 11:01:05', '2016-12-10 11:01:05'),
(5, 'Jorge Jason Xavier', 'georgina@ymail.com', '$2y$10$J6i9ARNdm.qBSM8i.y8S1u21gBYCbDyTZnLVhNSV9nZgVxoDrF67y', NULL, '2016-12-10 11:01:49', '2016-12-10 11:01:49'),
(6, 'Sherlock Holmes', 'artofdeduction@gmail.com', '$2y$10$5G2A8fQ76WvO3EDzlL0Eru5dV57mjUGYw1zVfX2emtWaUGSOgmktW', NULL, '2016-12-10 11:02:27', '2016-12-10 11:02:27'),
(7, 'John Watson', 'john221@gmail.com', '$2y$10$OaPjv1idHK2rtKyIiK8PkOZTC4AlkX52SU5CAFxKoX.pdalU2MY02', NULL, '2016-12-10 11:03:03', '2016-12-10 11:03:03'),
(8, 'Melanie Capute', 'melanie_caps@gmail.com', '$2y$10$FOFwLhUrZb4iKGLybk2uDOVk0qgYa/CTLXQk2QuQYgeHTWaPJcNii', 'wAwfOq0EFbkT1hhV0niZnoCXDmHUGeMbPxvzgwckqbpt2is467HRIDBP8k4a', '2016-12-11 17:53:22', '2016-12-11 18:06:55'),
(9, 'Sheilo Cabrera', 'mat_aljames92@gmail.com', '$2y$10$bElxOKMZQAkbQnkbElnR2eusweO.cJ40bk1Ei/0RaFS5jy1i6BeWK', 'zwwqA0BrsNpASqd9yPdZBvigzsyXYZMsgn2yPPbVHOLAVtm8vGotHFZLa7sI', '2016-12-11 18:07:41', '2016-12-11 18:12:32'),
(10, 'Jay qualine Tiu', 'jayqualine@rocketmail.com', '$2y$10$hScyTed1xZOhwUtYeNAeTeedGxEgXpWZuJojb92aVm5pA.yNaPeIy', '5z7ZIeCbGZUhjAylond84t3pZCK3N1MNMNsmVI7uC29QyrLbiF9lc7AsZK2G', '2016-12-11 18:13:12', '2016-12-11 18:13:19'),
(11, 'Mary Elaine Remarca', 'nikkie_chix100@gmail.com', '$2y$10$FAGmX7IUWZGd1Tm6POWpvOFGmR3SbVk89heU/zjSSC4jY3LcgsZpy', 'fEOg1yv03bruxU8T4e2nu6TIgibztUG2L8xc2oXcCfVp3RO77myxuVcbSLA0', '2016-12-14 17:16:26', '2016-12-14 17:16:46');

INSERT INTO `blogs` (`id`, `user_id`, `title`, `body`, `created_at`, `updated_at`, `date_added`) VALUES
(3, 2, 'I''m not actually a blogger', 'Lorem ipsum dolor amit sigfred nova elisi deh. \r\nLorem ipsum dolor amit sigfred nova elisi deh. \r\nLorem ipsum dolor amit sigfred nova elisi deh. \r\nLorem ipsum dolor amit sigfred nova elisi deh. ', '2016-12-10 11:09:02', '2016-12-16 12:20:19', '2016-12-10 03:09:02'),
(4, 2, 'Blog title #1', 'Hi. This is me, blogging. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2016-12-10 11:09:02', '2016-12-10 11:09:02', '2016-12-10 03:12:21'),
(5, 4, 'What I''ve been up to lately', 'Just playing with laravel. It''s fun actually, coding all day like playing XBOX games. But it''s better than to code than to waste your time with pointless games. Right?', '2016-12-10 11:09:02', '2016-12-17 15:35:31', '2016-12-10 03:12:21'),
(10, 1, 'This is a sample title.', 'Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. ', '2016-12-10 17:18:26', '2016-12-10 17:18:26', '2016-12-10 09:18:26'),
(11, 1, 'Hello world DzzzzzaahhHh!', 'I''m not a programmer by the way.', '2016-12-11 17:54:45', '2016-12-17 15:55:30', '2016-12-11 09:54:45'),
(12, 1, 'Hahahahahahaha', 'Hehehehehe. Hehehehehe. Hehehehehe. Hehehehehe. Hehehehehe. ', '2016-12-14 09:27:32', '2016-12-16 11:29:03', '2016-12-14 01:27:32'),
(13, 1, 'Hahahahahah', 'BlogAuthBlogAuthBlogAuth', '2016-12-14 09:29:45', '2016-12-14 09:29:45', '2016-12-14 01:29:45'),
(14, 2, 'This is UPDATED motherfucking shit', 'Hahahahaha. Hahahahaha. Hahahahaha. Hahahahaha. ', '2016-12-14 16:57:52', '2016-12-16 07:17:27', '2016-12-14 08:57:52'),
(15, 1, 'This is another titlle. BAZINGGA!!', 'UPDATED: Just a sample description...', '2016-12-16 11:24:57', '2016-12-16 11:26:13', '2016-12-16 03:24:57');

