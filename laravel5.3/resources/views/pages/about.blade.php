@extends('master')
@section('title', 'About')
@section('container')

	<div class='col-md-6'>
		<h2>About</h2>
		<p>
			About this laravel app, this is actually just a practice for their latest version of the framework.
			And now, I'm getting good at it. Before I got to practice coding in this version (5.3.16),
			I first started coding in 5.1. I begun coding at <strong> {{ $dev_date }} </strong> at <strong> {{ $dev_time }} </strong>.
		</p>
	</div>

@endsection