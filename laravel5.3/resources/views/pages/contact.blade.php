@extends('master')
@section('title', 'Contact')

@section('container')

	<div class='col-md-6'>

		<h2>Contact</h2>

		<h4> Hello, you can also contact me with the information given below: </h4>
		
		<div class='form-group'>
			<strong>Phone:</strong> {{ $phone_no }} <br />
			<strong>Email:</strong> eminence43rd@gmail.com <br />
		</div>

		<div class='form-group'>
			These are a list of frameworks I know how to use: <br />

			<ul>
				@foreach($frameworks as $framework)
					<li> {{ $framework }} </li>
				@endforeach
			</ul>
		</div>
	</div>

@endsection