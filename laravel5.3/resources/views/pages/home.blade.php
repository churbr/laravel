@extends('master')

@section('title')
	Home
@endsection

@section('container')

	<h3>Welcome to Laravel 5.3</h3>
	<p>
		Hi, my name is <strong> {{ $owner }}</strong> and I am the owner of this application created in laravel 5.3.
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed do eiusmod tempor incididunt ut labore.
		Et dolore magna aliqua.

		@if(Auth::check())
			Send me an email: {{ $auth->email }}.
		@endif

		Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris. Nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Cillum dolore eu fugiat nulla pariatur. By the way, I am already {{ $myAge }} years old. Yes, @age([1991, 4, 3]). I have friends and their names are @nice_name(Jerry), @nice_name(Tom) and @nice_name(Kim).
		Excepteur sint occaecat cupidatat non proident, sunt in culpa qui. Officia deserunt mollit anim id est laborum.
	</p>
	
@endsection