@extends('master')
@section('container')

	<div class='row'>
		<div class='col-md-6 col-md-offset-3'>
			<ul class='list-group'>
				
				@forelse($users as $user)
					<li class='list-group-item'>
						
						<span> {{ $user->name }} </span>
						<span class='pull-right clearfix'>
							Joined {{ $user->created_at->diffForHumans() }}
							<button class='btn btn-xs btn-primary'>Follow</button>
						</span>

					</li>
				@empty
					<p class='text-danger'>No users available.</p>
				@endforelse

				{{ $users->links() }}
			</ul>
		</div>
	</div>

@endsection