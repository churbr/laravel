@extends('master')
@section('title', 'View: ' . $user->name)
@section('container')
	
	<div class='row'>
		<div class='col-md-6 col-md-offset-3'>
			<h3> {{ $user->name }} </h3>

			<ul class='list-group'>
				
				<li class='list-group-item'> <strong>E-mail: </strong> {{ $user->email }} </li>
				<li class='list-group-item'> <strong>Birthday: </strong> {{ $user->birthday->format('F d Y, l') }} </li>
				<li class='list-group-item'> <strong>Age: </strong> @age([1991, 4, 3]) </li>
				<li class='list-group-item'> <strong>Date Registered: </strong> {{ $user->created_at->diffForHumans() }} </li>

			</ul>
		</div>
	</div>

@endsection