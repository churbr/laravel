<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::group(['middleware' => 'mymiddleware'], function() {
// Route::group(['middleware' => ['mymiddleware', 'anothermiddleware', 'samplemiddleware']], function() {
	Route::get('profile', 'ProfileController@index');
	Route::get('profile/{username}', 'ProfileController@profile');
	Route::get('users', 'MainController@userlist');
});

Route::get('about', 'MainController@about');
Route::get('contact', 'MainController@contact');
// Route::get('users', 'MainController@userlist')->middleware('mymiddleware');
Route::get('test', 'MainController@test');

Auth::routes(); // \vendor\laravel\framework\src\Illuminate\Routing\Router.php : auth() method
Route::get('/home', 'MainController@index');