<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        return view('pages.home');
    }

    public function register()
    {
        return view('pages.register');
    }

    public function login() {
        return view('pages.home');
    }
}
