<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Post;
use App\Like;

class PostController extends Controller
{

	public function create_post(Request $request) {

		$this->validate($request, [
			'body' => 'required|min:10|max:500',
		]);

		$post = new Post();
		$post->body = $request['body'];
		$message = 'Unable to post, something went wrong!';

		if($request->user()->post()->save($post)) {
			$message = 'Post successfully created!';
		}

		return redirect('profile')->with(['message' => $message]);
	}


	public function delete_post($post_id) {
		$post = Post::find($post_id);

		if(Auth::user() != $post->user) {
			return redirect()->back();
		}

		$post->delete();
		return redirect()->route('profile')->with(['message' => 'Post deleted!']);
	}

	public function update_post(Request $request) {
		// return response()->json(['message' => $request['body']]);

		$this->validate($request, [
			'body' => 'required|min:10|max:500'
		]);

		$post = Post::find($request['post_id']);
		$post->body = $request['body'];
		$post->update();

		/**
		 *	If you want to display the error messages, just append the loop through '$errors->all()'
		 *	and append to 'message' associative array.
		 **/

		return response()->json(['message' => $request['body']], 200); // 200 is a status code, means OK
	}

	public function likePost(Request $request) {

		$update  = false;				// Use for checking if there's already a record in the database, false by default
		$post_id = $request['post_id'];
		$is_like = $request['like'] === 'true' ? true : false;
										// true = like, false = dislike

		$post = Post::find($post_id);	// Find the post
		if(!$post) { return null; }		// If there's no post matched, just return or stop the process

		$user = Auth::user();			// Retreive the current authenticated user

		/** Check if the user already like this post
		 * $user 		- Take the currently logged in user.
		 * ->likes()	- Use the likes relation we set up in the User Model, which is likes() method to get all the likes user had
		 * ->where('post_id', $post_id)
		 				- And then I want to see from the likes of this user, or in all the likes this user did in the past
		 				- I want to find one, where the post_id of that like matches the post_id of the post
		 				- I'm now trying to like or dislike
		 * ->first()	- This is just to get 1 result
		 **/
		$like = $user->likes()->where('post_id', $post_id)->first();

		/** Check if there's really a record from the likes table.
		 *	If there's no record, then you have liked or dislike the post at all.
		 *	Or you have but you just undo what you did.
		 **/

		if($like) {
			// If there is indeed a record, we then check if it's either a like/dislike (true or false, 0 or 1)
			$already_like = $like->like;	// Using '$like' object which is an instance of App\Like Model
											// We access the property|column|field value of 'like' in the likes table using '->like'
											// true or 1 if like && false or 0 if disliked

			$update = true;					// Now, we can set $update to true which is previously set to false
											// Now, we set it to true because we already found a record and we're going to change it


			/**
			 * Now here, we check if the user already liked the post and then still clicks the first button which is 'like' button.
			 * That means, that the user wants to unlike it. Because when you click like, it will change to 'Unlike'.
			 * So when a user click it, $is_like (true) is equal to db record $already_like (true)
			 * So the logic is to check if $is_like which is from user click event
			 * matched the specific record from database $already_like.
			 **/

			if($already_like == $is_like) {
				$like->delete();			// We delete it because we don't want to change it to like or dislike
				return null;				// To end the process
			}
		} else {
			$like = new Like();				// Create an instance of Like and don't forget to use it (use App\Like)
		}

		// -----------------------------------------------------------------------------------------------------------------------

		$like->post_id = $post->id;
		$like->user_id = $user->id;
		$like->like = $is_like;

		if($update) {
			$like->update();
		}else {
			$like->save();
		}

		return null;

		// -----------------------------------------------------------------------------------------------------------------------
	}
}
