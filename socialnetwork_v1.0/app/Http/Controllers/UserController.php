<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Post;

class UserController extends Controller
{

	public function postRegister(Request $request) {

		$this->validate($request, [
			'username' => 'required|unique:users|min:5|max:30',
			'password' => 'required|alpha_num|min:6|max:35|confirmed',
			'firstname' => 'required|min:2|max:30|alpha',
			'lastname' => 'required|min:2|max:20|alpha',
			'birthday' => 'required|date',
		]);
		
		$username = $request['username'];
		$password = $request['password'];
		$firstname = $request['firstname'];
		$lastname =  $request['lastname'];
		$birthday = $request['birthday'];

		$user = new User();

		$user->username = $username;
		$user->password = $password;
		$user->firstname = $firstname;
		$user->lastname = $lastname;
		$user->birthday = $birthday;

		if( $user->save() ) {
			// Auth::login($user);
			// return redirect()->back();

			flash()->overlay( "<p>You have successfully registered.</p>
				<p>You can now login with your username & password!</p>", 'Success');
			
			return redirect('/');
		}


		
	}

	public function postSignIn(Request $request) {

		$username = $request['username'];
		$password = $request['password'];

		if( Auth::attempt(['username' => $username, 'password' => $password]) ) {
			return redirect()->route('profile');
		}else {
			// flash('Message', 'info')
			// flash('Message', 'success')
			// flash('Message', 'warning')
			// flash()->overlay('Modal Message', 'Modal Title')
			// flash('Message')->important()

			flash('Invalid credentials', 'danger');
			return redirect()->back();
		}

	}

	public function profile() {
		$posts = new Post();
		$posts = $posts::latest()->get();
		// $posts = $posts::all();
		
		return view('user.profile', compact('posts'));
	}

	// ---------------------------------------- File upload ----------------------------------------

	public function setting() {
		return view('account.setting', ['user' => Auth::user()]);
	}

	public function saveSetting(Request $request) {

		$this->validate($request, [
			'firstname' => 'required|regex:#^[a-zA-Z-\s]{2,}$#',
			'image' => 'image|mimes:jpeg,jpg,png,',
		]);

		$user = Auth::user();
		$user->firstname = $request['firstname'];

		// -------------------- Make Random filename --------------------

		$characters = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,,q,r,s,t,u,v,w,x,y,z,1,2,3,4,5,6,7,8,9,0';
		$characters_array = explode(',', $characters);
		shuffle($characters_array);
		$randomed_filename = implode('', $characters_array);

		// --------------------------------------------------------------

		if (Input::hasFile('image')) { // use Illuminate\Support\Facades\Input;
		    $file = Input::file('image');
		    $extension = $file->getClientOriginalExtension();
		    $filename = $randomed_filename . '.' . $extension;

			Storage::disk('user_image')->put($filename, File::get($file));
			$user->picture = $filename;
		}

		$user->update();

		return redirect()->route('account-setting');
	}

	public function getUserImage($filename) {
		$file = Storage::disk('user_image')->get($filename);
		return new Response($file, 200);
	}

	// ---------------------------------------- End of file upload ----------------------------------------

	public function logout() {
		Auth::logout();
		return redirect('/');
	}

}
