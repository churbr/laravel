<?php

namespace App;
use App\User;
use App\Post;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
	/* A like belongs to one user and one post. */

	public function user() { // This like belongs to a particular user who liked it
		return $this->belongsTo('App\User');
	}

	public function post() { // This like is attached to one post which is liked by the user
		return $this->belongsTo('App\Post');
	}
}
