<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $fillable = ['user_id', 'body'];

	public function user() { // This post belongs to a user
		return $this->belongsTo('App\User');
	}

	public function likes() { // This post can have many likes
		return $this->hasMany('App\Like');
	}

}
