<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;

class User extends Model implements Authenticatable
{

	use \Illuminate\Auth\Authenticatable; // UserController.php, line: 36

	protected $fillable = ['username', 'firstname', 'lastname', 'birthday'];
	protected $hidden = ['password', 'remember_token'];
	
    public function setPasswordAttribute($value) {
    	$this->attributes['password'] = bcrypt($value);
    }

    public function post() { // This user has many posts
    	return $this->hasMany('App\Post');
    }

    public function likes() { // This user has liked many posts
        return $this->hasMany('App\Like');
    }

}
