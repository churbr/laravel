<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table) {
            // $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('username', 32)->unique;
            $table->string('password');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('picture')->default('default.png');
            $table->date('birthday');
            $table->enum('type', ['admin', 'member'])->default('member');
            $table->boolean('status')->default(true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
