var post_id = 0;
var postBodyElement = null; // Declare here to make it global, so we can use with other functions

$('#flash-overlay-modal').modal();

$('div.alert').not('.alert-important').delay(3000).fadeOut(350);

$('.post-body').find('.interactions').find('.edit').on('click', function(e) {
	e.preventDefault();

	// event.target.parentNode.parentNode.parentNode.parentNode.nodeName; Check: profile.blade.php
	postBodyElement = e.target.parentNode.parentNode.childNodes[3];
	var body = postBodyElement.textContent;
	post_id = e.target.parentNode.parentNode.dataset['post_id']; // Declared in line: 1 in myjs.js

	$('textarea#body').val(body);
	$('#editpost_modal').modal();
});

$('#update-post').on('click', function() {
	
	// Check profile.blade.php; line: 47 & 48;
	// var editURL = '{{ route('update-post') }}';
	// var token = '{{ Session::token() }}';

	$.ajax({
		method: 'POST',
		url: editURL, 
		data: { body: $('textarea#body').val(), post_id: post_id, _token: token }
	})
	.done(function(result) {
		// console.log(result['message']);
		$(postBodyElement).text(result['message']);
		$('#editpost_modal').modal('hide');
	});
});


$('.like').on('click', function(e) {

	e.preventDefault();

	/**
	 *	<div class='interactions'>
	 *		<a href="#" class="like">Like</a> 1st link
	 *		<a href="#" class="like">Dislike</a> 2nd link
	 *	</div>

	 	The purpose of 'previousElementSibling' is to check if this particular element
	 	from event/clicked has a previous sibling. So, if we click the first link 'like',
	 	it will return true because there is no previous sibling (null).
	 	If we click the second link, which is the 'Dislike', it will return false because
	 	previous sibling is not equal to null, it's equal to null because there is a first
	 	sibling which is 'Like'.

	 	With that, we can determine whether the user click like or dislike.
	 **/

	post_id = e.target.parentNode.parentNode.dataset['post_id'];
 	var isLike =  e.target.previousElementSibling == null ? true : false;

	/**
	 *	var likeURL = '{{ route('like-post') }}';
	 *	var token = '{{ Session::token() }}';
	 **/

 	$.ajax({
 		method: 'POST',
 		url: likeURL,
 		data: { like: isLike, post_id: post_id , _token: token }
 	})
 	.done(function(result) {

 		var click = e.target.innerText.trim();
 		console.log(click);

 		if(click == 'Like' || click == 'You like this') {

 			if(click == 'Like') {
 				e.target.innerText = 'You like this';
 			} else { e.target.innerText = 'Like'; }

 			e.target.nextElementSibling.innerText = 'Dislike';
 		}else {

 			if(click == 'Dislike') {
 				e.target.innerText = 'You dislike this';
 			}else { e.target.innerText = 'Dislike'; }

 			e.target.previousElementSibling.innerText = 'Like';
 		}

 	});

});