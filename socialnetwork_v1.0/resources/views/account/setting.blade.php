@extends('master')
@section('container')

    <section class="row new-post">
        <div class="col-md-6 col-md-offset-3">
            <header><h3>Your Account</h3></header>
            <form action="{{ route('user-update') }}" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" name="firstname" class="form-control" value="{{ $user->firstname }}" id="first_name">
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" name="image" class="form-control" id="image">
                </div>
                <button type="submit" class="btn btn-primary">Save Account</button>
                <input type="hidden" value="{{ Session::token() }}" name="_token">
            </form>
        </div>
    </section>

    @if (Storage::disk('user_image')->exists($user->picture))
        <section class="row new-post">
            <div class="col-md-6 col-md-offset-3">
                <img class="img-responsive" src="{{ route('user.image', ['filename' => $user->picture]) }}" style="border-radius: 15%; width: 200px;">
            </div>
        </section>
    @endif

@endsection