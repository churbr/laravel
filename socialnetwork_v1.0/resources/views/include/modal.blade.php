<div class="modal fade" tabindex="-1" role="dialog" id='editpost_modal'>
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <form action="" method='POST'>
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit post</h4>
        </div>

        <div class="modal-body">
          <div class='form-group'>
            <textarea id='body' name='body' class='form-control' rows='5'></textarea>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id='update-post'>Save changes</button>
        </div>
      </form>

    </div>
  </div>
</div>