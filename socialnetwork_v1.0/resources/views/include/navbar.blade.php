<header>
	<nav class="navbar navbar-default navbar-static-top">
	    <div class="container-fluid">
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
	                <span class="sr-only">Toggle Navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>

	            <a class="navbar-brand" href="/">
	               Laravel
	            </a>
	        </div>



	        <div class="collapse navbar-collapse" id="app-navbar-collapse">
	            
	            <!-- Left Side Of Navbar -->
	            <ul class="nav navbar-nav">
	            	@if(Auth::guest())
		                <li> <a href="/">Home</a> </li>
		                <li> <a href="#">Contact Us</a> </li>
	            	@endif
	            </ul>

	            <!-- Right Side Of Navbar -->
<!-- 	            <ul class="nav navbar-nav navbar-right">
	            	@if(Auth::check())
	            		<li> <a href='#'> {{ Auth::user()->firstname }} {{ Auth::user()->lastname }} </a> </li>
	            		<li> <a href="{{ route('logout') }}"> <span>Logout</span> </a> </li>
	            	@endif

	            </ul> -->

	            @if(Auth::check())
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> {{ Auth::user()->firstname }} <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('account-setting') }}">Setting</a></li>
								<li><a href="{{ route('logout') }}">Logout</a></li>
							</ul>
						</li>
					</ul>
	            @endif


	        </div>
	    </div>
	</nav>
</header>