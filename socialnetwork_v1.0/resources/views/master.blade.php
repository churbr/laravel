<!DOCTYPE html>
<html>
	<head>
		<title>Script Kiddie</title>
		<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" >
		<link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/mycss.css') }}">
	</head>

	<body>
		
		
		<div class='container'>
			@include('include.navbar')
			@include('include.message-block')
			@include('flash::message')
			@yield('container')
			@include('include.footer')
		</div>

		@include('include.modal')


		<!-- This is only necessary if you do Flash::overlay('...') -->
		<script src="{{ asset('js/jquery-1.12.0.min.js') }}"></script>
		<script src="{{ asset('js/jquery-migrate-1.2.1.min.js') }}"></script>
		<script src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('js/myjs.js') }}"></script>
	</body>
</html>