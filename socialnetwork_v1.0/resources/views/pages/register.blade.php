@extends('master')
@section('title', 'Sign Up')
@section('container')

<div class='row'>

	<div class='col-md-6'>
		<form action="{{ route('registration') }}" method='POST'>
			 {{ csrf_field() }}

			 <h1>Sign Up</h1>

			<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
				<label for='username'>Username</label>
				<input class='form-control' type='text' name='username' value="{{ old('username') }}" />

                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
			</div>


			<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
				<label for='password'>Password</label>
				<input class='form-control' type='password' name='password' value="{{ old('password') }}" />

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
			</div>

			<div class="form-group">
				<label for='password'>Confirm-password</label>
				<input class='form-control' type='password' name='password_confirmation' />
			</div>

			<div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
				<label for='firstname'>Firstname</label>
				<input class='form-control' type='text' name='firstname' value="{{ old('firstname') }}" />

                @if ($errors->has('firstname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('firstname') }}</strong>
                    </span>
                @endif
			</div>

			<div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
				<label for='lastname'>Lastname</label>
				<input class='form-control' type='text' name='lastname' value="{{ old('lastname') }}" />

                @if ($errors->has('lastname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('lastname') }}</strong>
                    </span>
                @endif
			</div>

			<div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
				<label for='birthday'>Birthday</label>
				<input class='form-control' type='date' name='birthday' value="{{ old('birthday') }}" />

                @if ($errors->has('birthday'))
                    <span class="help-block">
                        <strong>{{ $errors->first('birthday') }}</strong>
                    </span>
                @endif
			</div>

			<div class='form-group'>
				<button class='btn btn-primary'>Register</button>
			</div>

		</form>
	</div>
	
</div>

@endsection