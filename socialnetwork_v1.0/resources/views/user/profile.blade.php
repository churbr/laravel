@extends('master')
@section('container')

	<section class='row new-post'>
		<div class='col-md-6 col-md-offset-3'>
			<form action="{{ route('createpost') }}" method='POST'>
				{{ csrf_field() }}
				<div class='form-group'>
					<textarea class='form-control' name='body' id='new-post' rows='5' placeholder="What's on your mind?"></textarea>
				</div>
				<button type='submit' class='btn btn-primary pull-right'>Post</button>
			</form>
		</div>
	</section>

	<section class="row user-post"> <!-- event.target.parentNode.parentNode.parentNode.parentNode.nodeName -->
		<div class='col-md-6 col-md-offset-3'> <!-- event.target.parentNode.parentNode.parentNode -->
			<header> <h4>Feed:</h4></header>

			@forelse($posts as $post)

				<article class='post-body' data-post_id="{{ $post->id }}"> <!-- event.target.parentNode.parentNode -->
					<p>{{ $post->body }}</p>
					<div class='info'>
						Posted by {{ $post->user->firstname }} on {{ $post->created_at }}
					</div>

					<div class='interactions'>
						<a href="#" class="like">
							@if(Auth::user()->likes()->where('post_id', $post->id)->first())
								@if(Auth::user()->likes()->where('post_id', $post->id)->first()->like == 1)
									You like this
								@else
									Like
								@endif
							@else
								Like
							@endif
						</a> |

						<a href="#" class="like">
							@if(Auth::user()->likes()->where('post_id', $post->id)->first())
								@if(Auth::user()->likes()->where('post_id', $post->id)->first()->like == 0)
									You dislike this
								@else
									Dislike
								@endif
							@else
								Dislike
							@endif	
						</a> |

						@if(Auth::user() == $post->user)
							<a class='edit' href="#">Edit</a> |
							<a href="{{ route('deletepost', ['post_id' => $post->id]) }}">Delete</a>
						@endif
					</div>
				</article>

			@empty
				<p>No post available yet.</p>
			@endforelse

		</div>
	</section>

	<script type="text/javascript">
		var editURL = '{{ route('update-post') }}'; // web.php, line: 62
		var likeURL = '{{ route('like-post') }}'; // web.php, line: 99
		var token = '{{ Session::token() }}';
	</script>

@endsection