<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'MainController@index')->middleware('mymiddleware');
Route::get('register', 'MainController@register');

Route::post('auth/register',
	[
		'as' => 'registration',
		'uses' => 'UserController@postRegister'
	]
);

Route::post('auth/signin',
	[
		'as' => 'signin',
		'uses' => 'UserController@postSignIn'
	]
);

Route::get('profile',
	[
		'as' => 'profile',
		'uses' => 'UserController@profile',
		'middleware' => 'auth' // Check /app/Http/Kernel.php; line: 49
	]
);

Route::post('post/create', 
	[
		'as' => 'createpost',
		'uses' => 'PostController@create_post',
		'middleware' => 'auth',
	]
);

Route::get('delete/post/{post_id}',
	[
		'as' => 'deletepost',
		'uses' => 'PostController@delete_post',
		'middleware' => 'auth',
	]
);

Route::get('logout',
	[
		'uses' => 'UserController@logout',
		'as' => 'logout',
	]
);

Route::post('update/post', 
	// function(Illuminate\Http\Request $request) { }
	// Receive $request['body'] from ajax and send it back using json response 'response()->json()'
	// return response()->json(['message' => $request['post_id']]); 
	[
		'uses' => 'PostController@update_post',
		'as' => 'update-post',
		'middleware' => 'auth',
	]
);

/* This section is for file upload routes */

Route::get('account/setting',
	[
		'uses' => 'UserController@setting',
		'as' => 'account-setting',
		'middleware' => 'auth',
	]
);

Route::get('user/image/{filename}', 
	[
		'uses' => 'UserController@getUserImage',
		'as' => 'user.image',
	]
);

Route::post('setting/update',
	[
		'uses' => 'UserController@saveSetting',
		'as' => 'user-update',
	]
);

/* End of file upload route section */

Route::post('like/post', 
	[
		'uses' => 'PostController@likePost',
		'as' => 'like-post',
		'middleware' => 'auth',
	]
);